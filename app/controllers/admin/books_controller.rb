class Admin::BooksController < Admin::BaseController

  before_action :load_book, only: [:edit, :update, :destroy]

  def index
    @books = Book.all
  end

  def new
    @book = Book.new
  end

  def create
    @book = Book.new book_params
    if @book.save
      redirect_to @book
    else
      render :new
    end
  end

  def edit
  end

  def update
    if @book.update book_params
      redirect_to @book
    else
      render :edit
    end
  end

  def destroy
    @book.destroy
    redirect_to admin_books_path
  end

  private

  def book_params
    params[:book].permit!
  end

  def load_book
    @book = Book.find params[:id]
  end

end
