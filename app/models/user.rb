class User < ActiveRecord::Base

  has_many :borrows

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable, :recoverable, :rememberable,
    :trackable, :validatable

  def is_admin?
    self.id == 1
  end

  def current_borrows
  end

  def past_borrows
  end

end
