class UsersController < ApplicationController
  before_action :authenticate_user!

  def show
  end

  def edit
  end

  def update
    if current_user.update user_params
      redirect_to current_user
    else
      render :edit
    end
  end

  private

  def user_params
    params[:user].permit :name, :address, :phone
  end

end
