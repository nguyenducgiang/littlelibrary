class Book < ActiveRecord::Base

  has_many :borrows

  validates :title, presence: true

  default_scope {order :title}

  state_machine initial: :available do

    state :unavailable
    state :available
    state :on_hold
    state :borrowed
    state :lost

    event :place_hold do
      transition available: :on_hold
    end

    event :borrow do
      transition on_hold: :borrowed
    end

    event :return do
      transition borrowed: :available
    end

    event :enable do
      transition all => :available
    end

    event :disable do
      transition all => :unavailable
    end

  end

end
