class CreateBooks < ActiveRecord::Migration
  def change
    create_table :books do |t|
      t.string :title
      t.string :author
      t.string :publisher
      t.string :year
      t.string :price
      t.string :image
      t.string :state
      t.string :isbn_10
      t.string :isbn_13
      t.text   :description

      t.timestamps
    end
  end
end
