class CreateBorrows < ActiveRecord::Migration
  def change
    create_table :borrows do |t|
      t.integer :book_id
      t.integer :user_id
      t.datetime :start_date
      t.datetime :end_date
      t.string :state

      t.timestamps
    end
  end
end
