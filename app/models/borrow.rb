class Borrow < ActiveRecord::Base

  belongs_to :book
  belongs_to :user

  after_create :change_book_state

  state_machine initial: :active do

    state :active
    state :returned
    state :overdue
    state :lost

  end

  private

  def change_book_state
    self.book.borrow
  end

end
